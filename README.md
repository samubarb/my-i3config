# .dotfiles
Settings files of my Arch Linux laptop.

![dotfiles-1](https://gitlab.com/samubarb/dotfiles/raw/media/dotfiles-1.png)
![dotfiles-1](https://gitlab.com/samubarb/dotfiles/raw/media/dotfiles-2.png)

## Keeping track of *dotfiles*
It's been very important to me keeping track of every single configuration file of my machine. The main short-term benefit is the ability to rollback to a previous configuration in case something goes wrong while tweaking the system.
I'm constantly putting effort into my system configuration, and I've managed to build a perfectly cozy, functional and productive environment for myself.

The idea that scares me is to loose all my settings when, one day, I will have to change my laptop, re-investing energy trying to replicate something whose memories have been lost over time.
It's not acceptable.

That's the real benefit, the long-term one.

Another aspect is the potential to be inspirational for someone else. All my *dotfiles* are a mix of feature that I needed, and I built, and features that I *stole* from other's configuration *dotfiles*.
Sharing this kind of things is what made my configuration of great value for me, it was precious to me so I'm giving back.

Use these *dotfiles* as you wish. 

## Dependencies
The main dependencies are `xorg`, `i3-gaps`, `polybar`, `compton` and `oh-my-zsh`.

If you are taking inspiration for just a piece of this repo, you don't have to install all of them, of course.
Just go for the ones linked to what you are trying to modify.

## Usage
For the correct usage of the `update` script, the repo must be cloned into `~/.dotfiles`. Otherwise you can change the `DIR` variable in the `update` file itself.

Using these *dotfiles* it's pretty straightforward... when you know what to do with them. If you are not familiar with [i3-gaps](https://wiki.archlinux.org/index.php/I3), [polybar](https://wiki.archlinux.org/index.php/Polybar) or [compton](https://wiki.archlinux.org/index.php/Compton), I suggest you to start reading their related documentation.
Are really well written and essential to start.

If you are a veteran of these things there's nothing relevant I have to say, I guess. Keep an eye on the `update` script I've made to collect all the *dotfiles* and put them in this folder.
It shows where every single file was taken from.

