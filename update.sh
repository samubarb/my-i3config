#!/usr/bin/env sh
#
# Update script that copy all dotfiles to be gitted

DIR="${HOME}/.dotfiles"

function col_output() {
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    NOCOLOR="\033[0m"


    # echo "CMD: ${1}"
    cmd_output=$(${1} 2>&1)
    # echo "EXIT CODE: ${?}"

    # echo " ${cmd_output}"
    [ $? -eq 0 ] && echo -e "${GREEN} Done${NOCOLOR}" ||
        echo -e "${RED} Failed${NOCOLOR}\n${cmd_output}"
}

# Copy Spotify configs in git folder
printf "Updating folder ${HOME}/.config/spotify"
col_output "cp -r ${HOME}/.config/spotify ${DIR}/"

# Copy polybar configs in git folder
printf "Updating folder ${HOME}/.config/polybar"
col_output "cp -r ${HOME}/.config/polybar ${DIR}/"

# # Copy awesome configs in git folder
# echo "Updating folder ${HOME}/.config/awesome"
# cp -r ${HOME}/.config/awesome ${DIR}/

# Copy i3 configs in git folder
printf "Updating folder ${HOME}/.config/i3"
col_output "cp -r ${HOME}/.config/i3 ${DIR}/"

# Copy picom configs in git folder
printf "Updating folder ${HOME}/.config/picom"
col_output "cp -r ${HOME}/.config/picom ${DIR}/"

# Copy pulseaudio configs in git folder
printf "Updating ${HOME}/.config/pulse"
col_output "cp ${HOME}/.config/pulse/daemon.conf ${DIR}/pulse/"
printf "Updating /etc/pulse/default.pa"
col_output "cp /etc/pulse/default.pa ${DIR}/pulse/"


# Copy xorg configs in git folder
printf "Updating files from /etc/X11/xorg.conf.d"
col_output "cp -r /etc/X11/xorg.conf.d ${DIR}/"

# Copy .xinitrc configs in git folder
printf "Updating ${HOME}/.xinitrc"
col_output "cp ${HOME}/.xinitrc ${DIR}/"

# Copy .Xresources configs in git folder
printf "Updating ${HOME}/.Xresources"
col_output "cp ${HOME}/.Xresources ${DIR}/"

# Copy .zshrc configs in git folder
printf "Updating ${HOME}/.zshrc"
col_output "cp ${HOME}/.zshrc ${DIR}/"

# Copy systemd custom services to git folder
printf "Updating custom user systemd units in ${HOME}/.config/systemd"
col_output "cp -r ${HOME}/.config/systemd/user/ ${DIR}/systemd/"

echo "Updating custom systemd unit /etc/systemd/system/:"
printf "  -- locknpause.service"
col_output "cp /etc/systemd/system/locknpause.service ${DIR}/systemd/system"

printf "  -- monitorsuspend.service"
col_output "cp /etc/systemd/system/monitorsuspend.service ${DIR}/systemd/system"

printf "  -- bluetoothsuspend.service"
col_output "cp /etc/systemd/system/bluetoothsuspend.service ${DIR}/systemd/system"

# Copy Breeze cursor theme settings
printf "Updating custom file .icons/default/index.theme"
col_output "cp ${HOME}/.icons/default/index.theme ${DIR}/cursor-theme/"
printf "Updating custom file .config/gtk-3.0/settings.ini"
col_output "cp ${HOME}/.config/gtk-3.0/settings.ini ${DIR}/cursor-theme/"

# Copy MX Master 2S `logid` config file
printf "Updating logid.cfg from /etc/ folder"
col_output "cp /etc/logid.cfg ${DIR}/etc/"

# Copy SSH config file
printf "Updating config from ${HOME}/.ssh"
col_output "cp ${HOME}/.ssh/config ${DIR}/ssh/"

# Show git status
echo ""
echo "Git status:" # newline
git status -s ${DIR}/
